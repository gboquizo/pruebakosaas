<?php

namespace App\Form;

use App\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('created_at',DateTimeType::class, [
                'input' => 'datetime',
                'with_seconds' => false,
                'html5' => true,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'combinedPickerInput',
                    'placeholder' => date('d/M/y H:m'),
                ],
                'format' => 'dd/MM/yyyy H:mm',
                'date_format' => 'd/m/Y H:mm',
                'translation_domain' => 'Default',
                'required' => false,
            ])
            ->add('tags', CollectionType::class, [
                'entry_type' => TagType::class,
                'entry_options' => [
                    'required'  => false,
                    'attr' => array (
                        'class' => 'form-control',

                    ),
                ],
                'label' => false,
                'allow_delete' => true,
                'allow_add' => true,
                'by_reference' => false,

            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }
}

