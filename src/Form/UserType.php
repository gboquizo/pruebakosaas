<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class,  ['required'   => true, 'empty_data' => 'example@example'])
            ->add('first_name', TextType::class, ['required'   => true, 'empty_data' => 'firstName'])
            ->add('last_name', TextType::class, ['required'   => true, 'empty_data' => 'lastName'])
            ->add('address', TextType::class, ['required'   => false, 'empty_data' => 'address'])
            ->add('phone_number', NumberType::class, ['required'   => false, 'empty_data' => ''])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'mapped'=> false,
                'first_options' => ['label' => 'Password'],
                'second_options' => ['label' => 'Confirm Password'],
                'invalid_message' => 'The password fields must match.',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}