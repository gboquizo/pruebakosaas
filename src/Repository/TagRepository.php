<?php

namespace App\Repository;

use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tag[]    findAll()
 * @method Tag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tag::class);
    }

    public function findSorted($tasks): array
    {
        $tasksArray = [];
        foreach ($tasks as $task) {
            $tasksArray[] = $task->getId();
        }

        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('t')
            ->from('App\Entity\Tag', 't')
            ->LeftJoin('t.tasks', 'ta')
            ->orderBy('ta.id', 'DESC' )
            ->getQuery()
            ->getResult();
    }

}
