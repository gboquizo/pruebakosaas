# Kosass Test Symfony 4

This project is developed under Symfony 4.4.

*In order to run the project, after cloning the repository, some previous steps are needed.*

1. Download and install [Composer](https://getcomposer.org/).
2. Download and install [Xampp](https://www.apachefriends.org/download.html) for PHP 7.4.23.
3. Download and install [Symfony CLI](https://symfony.com/download) .

---

## Deploy

After the previous requirements installation, is mandatory to run the next commands from the project's root path.

1. Run **composer install** for install all vendor depen.
2. Import the database gh_db into **Xampp's phpMyAdmin**.
3. A dummy user are being created for test purpose, could be used **test@test**, with pass *123*, for **login**.
4. In addition, any new user could be registered by clicking into **Register**

---

## Running the app

After login a user, the app could:

1. Show the **User's main page**, to see or edit user's data, manage the **Tasks List** (adding them to the logged user, or creating a new task), remove a task from the **User's Tasks** (Leave task) or manage the creation of tags for the task from **Tag List**
2. Show the **User List** page, to manage the creation / edition / delete of a current or new User.
3. Show the **Task List** page, to manage the creation / edition / delete of a current or new Task.
4. Show the **Tag List** page, to manage the creation / edition / delete of a current or new Tag.

---
